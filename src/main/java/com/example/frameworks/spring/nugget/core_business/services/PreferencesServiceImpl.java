package com.example.frameworks.spring.nugget.core_business.services;

import com.example.frameworks.spring.nugget.core_business.entities.Nugget;
import com.example.frameworks.spring.nugget.core_business.entities.Preference;
import com.example.frameworks.spring.nugget.core_business.entities.User;
import com.example.frameworks.spring.nugget.core_business.repositories.NuggetRepository;
import com.example.frameworks.spring.nugget.core_business.repositories.PreferencesRepository;
import com.example.frameworks.spring.nugget.core_business.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class PreferencesServiceImpl implements PreferencesService {
    private final NuggetRepository nuggetRepository;
    private final PreferencesRepository preferencesRepository;
    private final UserRepository userRepository;

    public PreferencesServiceImpl(NuggetRepository nuggetRepository, PreferencesRepository preferencesRepository,
                                  UserRepository userRepository) {
        this.nuggetRepository = nuggetRepository;
        this.preferencesRepository = preferencesRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void extractPreferences(String username, String preferencesAsCSV) {
        System.out.println(String.format("Extracting user preferences: username='%s' preferences='%s'",
                username, preferencesAsCSV));

        User user = userRepository.findByUsername(username);
        Set<String> preferences = Arrays.stream(preferencesAsCSV.split(","))
                .map(preference -> preference.toLowerCase().trim())
                .filter(preference -> !preference.isEmpty())
                .collect(Collectors.toSet());

        List<Nugget> nuggets = nuggetRepository.findAll();

        //The received SQL file with data seed for the Nuggets contains duplicate names.
        //Here duplicate nugget names are skipped from preferences.
        Set<String> foundPreferenceNames = new HashSet<>();

        //Here search is with time complexity O(N*P) which is slow if the number of preferences P
        //can be hypothetically a very large number > 200.
        //
        //There is a faster algorithm O(N*k) where k is constant = max_length(name) * (max_length(name) / 2).
        //1. Put each preference in HashSet --> preferencesHashSet.
        //2. For each nugget name
        //   1. Create all strings from nugget.name that are contained into that nugget.name.
        //      This is with time complexity O(1) if max_length(nugget.name) is fixed, i.e. max length <= 20 chars.
        //      For 20 chars, number of strings will be 20*(20/2) = 200 strings.
        //      This will have better time complexity only if the number of preferences is > 200.
        //   2. For each string check preferencesHashSet.contains(string).
        //   3. If contains add nugget.name to preferences.
        //3. End.
        for (Nugget nugget : nuggets) {
            if (foundPreferenceNames.contains(nugget.getName())) {
                //It is duplicate nugget name, skip it.
                continue;
            }
            for (String preference : preferences) {
                if (nugget.getName().toLowerCase().contains(preference)) {
                    preferencesRepository.save(new Preference(user, nugget));
                    foundPreferenceNames.add(nugget.getName());

                    //CONTAINS at least one preference in the nugget name - skip rest preferences.
                    break;
                }
            }
        }

        System.out.println("Done extracting user preferences!");
    }
}