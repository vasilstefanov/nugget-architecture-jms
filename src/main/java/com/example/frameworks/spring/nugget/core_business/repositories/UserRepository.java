package com.example.frameworks.spring.nugget.core_business.repositories;

import com.example.frameworks.spring.nugget.core_business.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);
}
