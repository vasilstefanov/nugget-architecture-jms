package com.example.frameworks.spring.nugget.core_business.services;

import com.example.frameworks.spring.nugget.core_business.models.NuggetModel;

import java.util.List;

public interface NuggetService {
    List<NuggetModel> getAllNuggets();
}
