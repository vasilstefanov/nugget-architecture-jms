package com.example.frameworks.spring.nugget.core_business.repositories;

import com.example.frameworks.spring.nugget.core_business.entities.Nugget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NuggetRepository extends JpaRepository<Nugget, String> {
}
