package com.example.frameworks.spring.nugget.client_application.services;

import com.example.frameworks.spring.nugget.core_business.models.UserRegistrationModel;

public interface MessageService {
    void sendUserRegistrationMessage(UserRegistrationModel userRegistrationModel);
}
