package com.example.frameworks.spring.nugget.core_business.services;

public interface PreferencesService {
    void extractPreferences(String username, String preferencesAsCSV);
}
