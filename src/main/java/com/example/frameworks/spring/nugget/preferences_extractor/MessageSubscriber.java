package com.example.frameworks.spring.nugget.preferences_extractor;

import com.example.frameworks.spring.nugget.core_business.services.PreferencesService;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;

@Component
public class MessageSubscriber {
    private final PreferencesService preferencesService;

    public MessageSubscriber(PreferencesService preferencesService) {
        this.preferencesService = preferencesService;
    }

    @JmsListener(destination = "user-registration")
    public void onMessageReceived(Message message) throws JMSException {
        if (message instanceof MapMessage) {
            MapMessage mapMessage = (MapMessage) message;
            String username = mapMessage.getString("username");
            String preferences = mapMessage.getString("preferences");
            preferencesService.extractPreferences(username, preferences);
        } else {
            System.out.println("Unknown message: " + message);
        }
    }
}

