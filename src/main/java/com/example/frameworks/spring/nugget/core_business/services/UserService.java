package com.example.frameworks.spring.nugget.core_business.services;

import com.example.frameworks.spring.nugget.core_business.models.PreferenceModel;
import com.example.frameworks.spring.nugget.core_business.models.UserRegistrationModel;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

public interface UserService {
    void registerUser(@Valid UserRegistrationModel user);

    default void validateRegistration(UserRegistrationModel registerUserModel, BindingResult bindingResult) {
        if (!Objects.equals(registerUserModel.getConfirmPassword(), registerUserModel.getPassword())) {
            bindingResult.rejectValue("password", "error.user.password",
                    "Password should match Confirm Password.");

            bindingResult.rejectValue("confirmPassword", "error.user.confirmPassword",
                    "Confirm Password should match Password.");
        }
    }

    List<PreferenceModel> getUserPreferences(String username);
}
