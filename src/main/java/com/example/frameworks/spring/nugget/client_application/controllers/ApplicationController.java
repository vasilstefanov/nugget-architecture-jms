package com.example.frameworks.spring.nugget.client_application.controllers;

import com.example.frameworks.spring.nugget.client_application.bindings.LoginBinding;
import com.example.frameworks.spring.nugget.client_application.services.MessageService;
import com.example.frameworks.spring.nugget.core_business.models.PreferenceModel;
import com.example.frameworks.spring.nugget.core_business.models.UserRegistrationModel;
import com.example.frameworks.spring.nugget.core_business.services.NuggetService;
import com.example.frameworks.spring.nugget.core_business.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class ApplicationController {
    private static class ViewName {
        static final String LOGIN = "login";
        static final String REGISTER = "register";
        static final String NUGGETS = "nuggets";
        public static final String PREFERENCES = "preferences";
    }

    private final UserService userService;
    private final NuggetService nuggetService;
    private final MessageService messageService;

    public ApplicationController(UserService userService, NuggetService nuggetService, MessageService messageService) {
        this.userService = userService;
        this.nuggetService = nuggetService;
        this.messageService = messageService;
    }

    @GetMapping("/login")
    public String login(@ModelAttribute("login") LoginBinding loginBinding) {
        loginBinding.setRememberMe(true);
        return ViewName.LOGIN;
    }

    @GetMapping("/register")
    public String register(@ModelAttribute("user") UserRegistrationModel userRegistrationModel) {
        return ViewName.REGISTER;
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute("user") UserRegistrationModel userRegistrationModel,
                               BindingResult bindingResult) {
        userService.validateRegistration(userRegistrationModel, bindingResult);
        if (bindingResult.hasErrors()) {
            return ViewName.REGISTER;
        }
        userService.registerUser(userRegistrationModel);
        messageService.sendUserRegistrationMessage(userRegistrationModel);
        return "redirect:/login";
    }

    @GetMapping("/nuggets")
    public String nuggets(Model model) {
        model.addAttribute("nuggets", nuggetService.getAllNuggets());
        return ViewName.NUGGETS;
    }

    @GetMapping("/preferences")
    public String preferences(Model model, Principal principal) {
        List<PreferenceModel> preferences = userService.getUserPreferences(principal.getName());
        model.addAttribute("preferences", preferences);
        return ViewName.PREFERENCES;
    }
}