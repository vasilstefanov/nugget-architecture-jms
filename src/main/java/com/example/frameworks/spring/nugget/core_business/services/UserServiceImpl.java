package com.example.frameworks.spring.nugget.core_business.services;

import com.example.frameworks.spring.nugget.core_business.entities.Nugget;
import com.example.frameworks.spring.nugget.core_business.entities.User;
import com.example.frameworks.spring.nugget.core_business.models.PreferenceModel;
import com.example.frameworks.spring.nugget.core_business.models.UserRegistrationModel;
import com.example.frameworks.spring.nugget.core_business.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
class UserServiceImpl implements UserService, UserDetailsService {
    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserServiceImpl(BCryptPasswordEncoder passwordEncoder, UserRepository userRepository, ModelMapper modelMapper) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public void registerUser(UserRegistrationModel userRegistrationModel) {
        User user = new User();

        user.setUsername(userRegistrationModel.getUsername());
        user.setPassword(passwordEncoder.encode(userRegistrationModel.getPassword()));

        userRepository.save(user);
    }

    @Override
    public List<PreferenceModel> getUserPreferences(String username) {
        User user = loadUserByUsername(username);
        List<Nugget> preferences = user.getPreferences();

        Type preferenceModelListType = new TypeToken<List<PreferenceModel>>() {
        }.getType();

        return modelMapper.map(preferences, preferenceModelListType);
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return user;
    }
}