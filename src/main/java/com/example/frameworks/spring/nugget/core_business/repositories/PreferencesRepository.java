package com.example.frameworks.spring.nugget.core_business.repositories;

import com.example.frameworks.spring.nugget.core_business.entities.Preference;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PreferencesRepository extends JpaRepository<Preference, String> {
}
