package com.example.frameworks.spring.nugget.client_application.services;

import com.example.frameworks.spring.nugget.core_business.models.UserRegistrationModel;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
class MessageServiceImpl implements MessageService {
    private final JmsTemplate jmsTemplate;

    public MessageServiceImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendUserRegistrationMessage(UserRegistrationModel userRegistrationModel) {
        final String username = userRegistrationModel.getUsername();
        final String preferences = userRegistrationModel.getPreferences();

        System.out.println(String.format("Sending user registration message: username='%s' preferences='%s'",
                username, preferences));

        Map<String, Object> message = new HashMap<>();
        message.put("username", username);
        message.put("preferences", preferences);

        jmsTemplate.convertAndSend("user-registration", message);
    }
}
