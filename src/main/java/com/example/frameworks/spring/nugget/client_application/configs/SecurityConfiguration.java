package com.example.frameworks.spring.nugget.client_application.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    private UserDetailsService userDetailsService;

    public SecurityConfiguration(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/css/**", "/js/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/register", "/login*")
                .permitAll()

                .antMatchers("/preferences", "/nuggets")
                .authenticated()

                .anyRequest()
                .denyAll();

        http.formLogin()
                .loginPage("/login")
                .permitAll();

        http.logout();

        http.rememberMe()
                .userDetailsService(userDetailsService)
                .rememberMeParameter("rememberMe")
                .tokenValiditySeconds(10 * 60);

        http.exceptionHandling()
                .accessDeniedPage("/unauthorized");

        http.csrf()
                .disable();
        // Default csrf parameter name "_csrf"
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/unauthorized").setViewName("unauthorized");
    }
}