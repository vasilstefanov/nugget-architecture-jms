package com.example.frameworks.spring.nugget.core_business.models;

import javax.validation.constraints.NotEmpty;

public class NuggetModel {
    @NotEmpty
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NuggetModel{" +
                "name='" + name + '\'' +
                '}';
    }
}