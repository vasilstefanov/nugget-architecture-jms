package com.example.frameworks.spring.nugget.core_business.services;

import com.example.frameworks.spring.nugget.core_business.entities.Nugget;
import com.example.frameworks.spring.nugget.core_business.models.NuggetModel;
import com.example.frameworks.spring.nugget.core_business.repositories.NuggetRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
class NuggetServiceImpl implements NuggetService {
    private final NuggetRepository nuggetRepository;
    private final ModelMapper modelMapper;

    public NuggetServiceImpl(NuggetRepository nuggetRepository, ModelMapper modelMapper) {
        this.nuggetRepository = nuggetRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<NuggetModel> getAllNuggets() {
        List<Nugget> nuggets = nuggetRepository.findAll();

        Type nuggetModelListType = new TypeToken<List<NuggetModel>>() {
        }.getType();

        return modelMapper.map(nuggets, nuggetModelListType);
    }
}